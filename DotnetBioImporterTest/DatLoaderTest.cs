namespace DotnetBioImporterTest
{
    using System;
    using System.Linq;
    using DotnetBioImporter.Services;
    using Xunit;

    public class DatLoaderTest
    {
        private readonly DatLoader _datLoader;

        public DatLoaderTest()
        {
            _datLoader = new DatLoader();
        }

        [Fact]
        public void CanLoad()
        {
            var inputTest = @"       
       41	2019-07-08 16:52:04	1	0	1	0
       96	2019-07-08 16:53:00	1	0	1	0
       41	2019-07-08 16:53:16	1	0	1	0
        1	2019-07-08 16:56:26	1	0	1	0
       41	2019-07-09 08:28:56	1	0	1	0
       58	2019-07-09 08:31:48	1	0	1	0
";
            var items = _datLoader.Load(inputTest);
            Assert.Equal(6, items.Count());
            Assert.Contains(items, x => (new long[] { 96, 41, 1, 58 }).Contains(x.CardId));
            Assert.Equal(636982015240000000, items.First().Timestamp.Ticks);
        }
    }
}
