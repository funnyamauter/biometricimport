namespace DotnetBioImporterTest
{
    using System.IO;
    using DotnetBioImporter;
    using Xunit;

    public class ProgramTest
    {
        [Theory]
        [InlineData("")]
        [InlineData("no any meaning")]
        public void CanReturnFaieldIfNoFileInput(string input)
        {
            var result = Program.Main(input);
            Assert.Equal(1, result);
        }

        [Fact]
        public void CanThrowExceptionIfInvalidFile()
        {
            Assert.Throws<FileNotFoundException>(() => Program.Main("-f nofile.dat"));
        }

        [Fact]
        public void CanCreateImportCode()
        {
            var result = Program.Main("-f test_01.dat");
            Assert.Equal(0, result);
            var sameResult = Program.Main("--file test_01.dat","-o newOutput.sql");
            Assert.Equal(0, sameResult);
        }
    }
}
