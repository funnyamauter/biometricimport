namespace DotnetBioImporterTest
{
    using System;
    using DotnetBioImporter.Entities;
    using DotnetBioImporter.Services;
    using Xunit;

    public class SqlImportBuilderTest
    {
        private readonly SqlImportBuilder _sqlImportBuilder;

        public SqlImportBuilderTest()
        {
            _sqlImportBuilder = new SqlImportBuilder("AttendanceRecords");
        }

        [Fact]
        public void CanLoadSql()
        {
            var date1 = new DateTime(2019, 12, 24, 8, 5, 3, 0, DateTimeKind.Utc);
            var date2 = new DateTime(2019, 12, 24, 7, 5, 3, 0, DateTimeKind.Utc);
            var date3 = new DateTime(2019, 12, 12, 7, 5, 3, 0, DateTimeKind.Utc);

            var items = new AttentionRecord[] { new AttentionRecord(1, date1), new AttentionRecord(2, date2), new AttentionRecord(3, date3) };

            var expected = "INSERT INTO AttendanceRecords(CardId, Timestamp, StampingType, DateCreated, DateUpdated, Processed) VALUES (1, 637127715030000000, 1, '2019-12-24 08:05:03.0000000Z', '2019-12-24 08:05:03.0000000Z', 0), (2, 637127679030000000, 1, '2019-12-24 07:05:03.0000000Z', '2019-12-24 07:05:03.0000000Z', 0), (3, 637117311030000000, 1, '2019-12-12 07:05:03.0000000Z', '2019-12-12 07:05:03.0000000Z', 0);";
            var actual = _sqlImportBuilder.BuildFrom(items);
            Assert.Equal(expected, actual);
        }
    }
}
