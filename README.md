# BioMetricImport
## Install dotnet 3.0
#### From Window using [Chocolatey](https://chocolatey.org/install)
```cmd
choco install dotnetcore-sdk 
```
#### From MacOS using [Homebrew](https://brew.sh)
```cmd
brew cask install dotnet-sdk
```
#### From Linux using [Package Manager](https://chocolatey.org/install)
```
sudo apt-get update
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install dotnet-sdk-3.1
```
## Run Project to create sql file
You can build the project if you want. But I prefer running using SDK.
```
cd DotnetBioImporter
dotnet run -f client_import.dat
```

You can only export the data from a specific time range.
```cmd
dotnet run --file test_01.dat --since 2019-01-13 --to 2020-12-04 
```

You can rename the output file. The default is `output.sql`.
```cmd
dotnet run -f client_import.dat -o doreming.sql 
```
## Run sql queries on client's PC
1. The sql is supposed to be placed at `%appdata%\BioMetrickClockIn\BioMetrickClockIn\1.0.0\`
2. Run the query file against the sql.
3. Restart Doreming application.


