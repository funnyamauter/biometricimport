﻿#nullable enable

namespace DotnetBioImporter.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DotnetBioImporter.Entities;
    using DotnetBioImporter.Interfaces;

    public class DatLoader : IDataLoader
    {
        public IEnumerable<AttentionRecord> Load(string data) => data.Split("\n").Select(Map).Where(x => x != null).Cast<AttentionRecord>().ToArray();

        private static AttentionRecord? Map(string data)
        {
            var rawItems = data.Trim().Split("\t");
            if (rawItems.Length < 3) return null;
            if (long.TryParse(rawItems[0], out var id) && DateTime.TryParseExact(rawItems[1], "yyyy-MM-dd HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out var dt))
            {
                return new AttentionRecord(id, dt);
            }

            return null;
        }
    }

}