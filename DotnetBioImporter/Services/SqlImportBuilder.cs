﻿namespace DotnetBioImporter.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DotnetBioImporter.Entities;
    using DotnetBioImporter.Interfaces;

    public class SqlImportBuilder : IImportCodeBuilder
    {
        private readonly string _tableName;

        public SqlImportBuilder(string tableName)
        {
            _tableName = tableName;
        }

        public string BuildFrom(IEnumerable<AttentionRecord> records) => $"{BuildInsertCommand(_tableName)} VALUES {BuildInsertValues(records)};";

        private static string BuildInsertCommand(string tableName) => $"INSERT INTO {tableName}(CardId, Timestamp, StampingType, DateCreated, DateUpdated, Processed)";
        private static string BuildInsertValue(AttentionRecord record) => $"({record.CardId}, {record.Timestamp.Ticks}, 1, '{GetTimeString(record.Timestamp)}', '{GetTimeString(record.Timestamp)}', 0)";
        private static string BuildInsertValues(IEnumerable<AttentionRecord> records) => string.Join(", ", records.Select(BuildInsertValue).ToArray());
        private static string GetTimeString(DateTime dt) => dt.ToString("yyyy-MM-dd HH\\:mm\\:ss.fffffffZ");

    }

}