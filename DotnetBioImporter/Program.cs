﻿#nullable enable
namespace DotnetBioImporter
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Linq;
    using DotnetBioImporter.Interfaces;
    using DotnetBioImporter.Services;
    using McMaster.Extensions.CommandLineUtils;

    public class Program
    {
        public static int Main(params string[] args) => CommandLineApplication.Execute<Program>(args);

        [Required]
        [Option(Description = "File path", ShortName ="f")]
        public string File { get; } = string.Empty;

        [Option(Description = "Since time", ShortName = "s")] 
        public DateTime Since { get; } = DateTime.MinValue;

        [Option(Description = "To time", ShortName = "t")] 
        public DateTime To { get; } = DateTime.MaxValue;

        [Option(Description = "Output file. Default is 'output.sql'", ShortName = "o")]
        public string Output { get; } = "output.sql";

        private void OnExecute()
        {
            using (StreamReader sr = new StreamReader(this.File))
            {
                var line = sr.ReadToEnd();
                var (loader, codeBuilder) = ProvideServices();
                var records = loader.Load(line);
                var importCode = codeBuilder.BuildFrom(records.Where(rec =>this.Since < rec.Timestamp && rec.Timestamp < this.To).ToArray());
                using (StreamWriter outputFile = new StreamWriter(this.Output))
                {
                    outputFile.WriteLine(importCode);
                }
            }
        }

        private (IDataLoader, IImportCodeBuilder) ProvideServices()
        {
            return (new DatLoader(), new SqlImportBuilder("AttendanceRecords"));
        }
    }
}