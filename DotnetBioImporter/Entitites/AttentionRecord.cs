﻿


namespace DotnetBioImporter.Entities
{
    using System;

    public class AttentionRecord
    {
        public AttentionRecord(long cardId, DateTime timestamp)
        {
            CardId = cardId;
            Timestamp = timestamp;
        }

        public long CardId { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
