﻿namespace DotnetBioImporter.Interfaces
{
    using System.Collections.Generic;
    using DotnetBioImporter.Entities;

    public interface IImportCodeBuilder
    {
        string BuildFrom(IEnumerable<AttentionRecord> records);
    }
}
