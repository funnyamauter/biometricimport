﻿namespace DotnetBioImporter.Interfaces
{
    using System.Collections.Generic;
    using DotnetBioImporter.Entities;

    public interface IDataLoader
    {
        IEnumerable<AttentionRecord> Load(string data);
    }
}
